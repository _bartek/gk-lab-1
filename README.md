# Lab 1
Pierwszy z czterech projektów przewidzianych na przedmiot Grafika Komputerowa
na wydziale MiNI Politechniki Warszawskiej w semestrze zimowym 2016/2017
Autor: Bartek Sielicki


## Czym jest projekt?
Jest to prosty graficzny edytor wielokątów, pozwalający na wykonanie podstawowych operacji, takich jak:
+ Rysowanie nowego wielokąta na planszy wierzchołek po wierzchołku
+ Usuwanie istniejących wielokątów
+ Wstawianie / usuwanie wierzchołków z/do istniejącego wielokąta
+ Przesuwanie wierzchołków należących do istniejącego wielokąta (w miarę możliwości)
+ Przesuwanie istniejących wielokątów
+ Nakładanie "ograniczeń" na istniejące krawędzie - wymuszenie równoległości do danej osi, bądź ustalanie długości

Projekt wykonany jest w formie aplikacji WWW z wykorzystaniem HTML5, CSS3 oraz języka EcmaScript 6.


## Co znajduje się w repozytorium?
Aplikacja WWW (w folderze www) oraz prosty serwer korzystajacy z node.js (express) pozwalający na szybkie jej uruchomienie. 

## Jak go uruchomić?
1. **Sposób 1 (zalecany)** - na komputerze musi być zainstalowany menadżer pakietów **npm** oraz serwer node.js wykonywalny jako **node**. Przed pierwszym uruchomieniem aplikacji należy zainstalować wymagane zależności poleceniem *npm install* wykonanym w nadrzędym katalogu projektu, a następnie uruchomienie serwera hostującego aplikację poleceniem *node server.js* - od tego momentu aplikacja będzie dostępna pod adresem wyświetlonym w konsoli do momentu zakończenia się procesu serwera.

2. **Sposób 2 (prostszy)** - należy wejść do katalogu *www* oraz przez wybraną przeglądarkę otworzyć plik *index.html* - sposób ten nie ma gwarancji działania na każdej konfiguracji sprzętowej / programowej 

## Opis zastosowanego algorytmu
1. **Przesuwanie wierzchołka w wielokącie z ograniczeniami**: Przy każdej zmianie stanu planszy wykonywana jest metoda *render()* na każdym wielokącie który się na niej znajduje (wymuszone to jest potrzebą czyszczenia całego płótna przy przerysowaniu czegokolwiek).
Ciało metody render poprzedza wywołanie metody *validate()* która "naprawia" wierzchołki, tak aby ich wzajemne położenie spełniało ograniczenia nałożone przez użytkownika na krawędzie wielokątu.
Metoda ta iteruje przez wszystkie krawędzie wielokąta (zaczynając od tej pomiędzy wierzchołkami 0 a 1) i poprawia współrzędne kolejnego wierzchołka względem poprzedniego.
Nie jest to co prawda metoda w 100% idealna, jednak bardzo prosta w implementacji i dająca wystarczające i proste do przewidzenia rezultaty w większości przypadków.
Nie jest jednak wykluczone że w przyszłości być może w miarę potrzeb będzie udoskonalana.
