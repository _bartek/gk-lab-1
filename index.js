'use strict';

// imports
var express = require('express');


// app
var app = express();
app.use(express.static('www'))

var server = app.listen(3000, function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log("Listening at http://%s:%s", host, port);
});
