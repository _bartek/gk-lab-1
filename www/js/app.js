Zepto(function($) {
	var $canvas = $('#app-canvas');
	var app = new CanvasWrapper($canvas[0]);

	$('.tool').on('click', function () {
		if ($(this).hasClass('disabled')) {
			return;
		}
		var state = $(this).data('state');
		if (state) {
			app.enterState(state);
		}
	});

	$('#wipe-tool').on('click', function () {
		if ($(this).hasClass('disabled')) {
			return;
		}
		app.clear();
	});

	$canvas.on('stateEntered', function (e) {
		var $buttons = $('.tool');
		var $button = $buttons.filter('[data-state="'+e.detail+'"]');

		if ($button.length) { // non-idle state
			$buttons.addClass('disabled');
			$button.addClass('active');
			$canvas.addClass('working-state');
		}
	});
	$canvas.on('stateExited', function () {
		$('.tool').removeClass('disabled active');
		$canvas.removeClass('working-state');
	});
});