class CanvasWrapper {
    /* wrapper for canvas objects */

    static get STATES() {
        return {
            idle: IdleState,
            addPolygon: AddPolygonState,
            movePolygon: MovePolygonState,
            removePolygon: RemovePolygonState,
            moveVertex: MoveVertexState,
            removeVertex: RemoveVertexState,
            addVertex: AddVertexState,
            addVerticalLineRestriction: AddVerticalLineRestrictionState,
            addHorizontalLineRestriction: AddHorizontalLineRestrictionState,
            addFixedLengthLineRestriction: AddFixedLengthLineRestrictionState,
            removeRestriction: RemoveRestrictionState,
            setBresenhamAlgorithm: SetBresenhamAlgorithmState,
            setXiaolinWuAlgorithm: SetXiaolinWuAlgorithmState,
            setEmbeddedAlgorithm: SetEmbeddedAlgorithmState,
            clipPolygon: ClipPolygonState,
            fillPolygon: FillPolygonState,
            addLight: AddLightState,
            removeLight: RemoveLightState,
            addBubble: AddBubbleState,
            removeBubble: RemoveBubbleState,
        }
    }

    static getDefaultState() {
        return CanvasWrapper.STATES.idle
    }

    static translatePosition(element, x, y) {
        /* translates position from absolute to relative to element */
        var rect = element.getBoundingClientRect();
        return {
            x: x - rect.left,
            y: y - rect.top,
        }
    }

    constructor(canvas) {
        this.canvas = canvas;
        this.elements = [];
        this.context = this.canvas.getContext("2d");

        this.texture = new Texture('wood.png', 500, 500);
        
        var state = CanvasWrapper.getDefaultState();
        this.state = new state(this);

        this.startDispatchingEvents();
    }

    enterState(stateName) {
        var state = CanvasWrapper.STATES[stateName];
        if (state != undefined) {
            this.state = new state(this);
        }

        this.canvas.dispatchEvent(
            new CustomEvent('stateEntered', {'detail': stateName})
        );
        console.log("State entered: " + stateName);
    }

    exitCurrentState() {
        var state = CanvasWrapper.getDefaultState();
        this.state = new state(this);

        this.canvas.dispatchEvent(
            new Event('stateExited')
        );
        console.log("State exited. Default state entered.");
    }

    addElement(element) {
        this.elements.push(element);
    }

    removeElement(element) {
        var idx = this.elements.indexOf(element);
        if (idx > -1) {
            this.elements.splice(idx, 1);
        }
    }

    clear() {
        this.elements = [];
        this.refresh();
    }
    
    refresh() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        for (var element of this.elements) {
            element.render(this.context, this.texture);
        }
    }

    getElementsForPosition(x, y) {
        var elements = [{
            item: this.canvas,
            selector: 'canvas'
        }];
        for (var i = this.elements.length - 1; i >= 0; i--) {
            var element = this.elements[i];
            var hitElements = element.getElementsForPosition(x, y)
            if (hitElements.length) {
                elements.push.apply(elements, hitElements);
                break;
            }
        }
        return elements;
    }

    dispatchEvent(eventName, event) {
        console.log("dispatching event: " + eventName);

        var position = CanvasWrapper.translatePosition(
            this.canvas, event.clientX, event.clientY
        );
        var elements = this.getElementsForPosition(position.x, position.y);
        var stateEvent = new StateEvent(position.x, position.y);
        while (elements.length && stateEvent.propagate) {
            var element = elements.pop();
            // set / switch event cargo
            stateEvent.item = element.item;
            this.state.triggerEvent(eventName, element.selector, stateEvent);
        }
        this.refresh();
    }

    startDispatchingEvents() {
        var self = this;

        this.canvas.addEventListener('stateChanged', function (e) {
            self.refresh();
        });
        this.canvas.addEventListener('click', function (e) {
            self.dispatchEvent('click', e);
        });
        this.canvas.addEventListener('mousedown', function (e) {
            self.dispatchEvent('mousedown', e);
        });
        this.canvas.addEventListener('mousemove', function (e) {
            self.dispatchEvent('mousemove', e);
        });
        this.canvas.addEventListener('mouseup', function (e) {
            self.dispatchEvent('mouseup', e);
        });
        this.canvas.addEventListener('contextmenu', function (e) {
            e.preventDefault();
            self.dispatchEvent('rightclick', e);
            return false;
        });
    }
}