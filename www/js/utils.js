// GEOMETRIC function to get the intersections
function segmentIntersectsCircle(a, b, c) {
	// a, b - endpoints of segment
	// c - center point and radius of circle

	// Calculate the euclidean distance between a & b
	eDistAtoB = Math.sqrt( Math.pow(b[0]-a[0], 2) + Math.pow(b[1]-a[1], 2) );

	// compute the direction vector d from a to b
	d = [ (b[0]-a[0])/eDistAtoB, (b[1]-a[1])/eDistAtoB ];

	// Now the line equation is x = dx*t + ax, y = dy*t + ay with 0 <= t <= 1.

	// compute the value t of the closest point to the circle center (cx, cy)
	t = (d[0] * (c[0]-a[0])) + (d[1] * (c[1]-a[1]));

	// compute the coordinates of the point e on line and closest to c
    var e = {coords:[], onLine:false};
	e.coords[0] = (t * d[0]) + a[0];
	e.coords[1] = (t * d[1]) + a[1];

	// Calculate the euclidean distance between c & e
	eDistCtoE = Math.sqrt( Math.pow(e.coords[0]-c[0], 2) + Math.pow(e.coords[1]-c[1], 2) );

	// test if the line intersects the circle
	if( eDistCtoE < c[2] ) {
		// compute distance from t to circle intersection point
	    dt = Math.sqrt( Math.pow(c[2], 2) - Math.pow(eDistCtoE, 2));

	    // compute first intersection point
	    var f = {coords:[], onLine:false};
	    f.coords[0] = ((t-dt) * d[0]) + a[0];
	    f.coords[1] = ((t-dt) * d[1]) + a[1];
	    // check if f lies on the line
	    f.onLine = is_on(a,b,f.coords);

	    // compute second intersection point
	    var g = {coords:[], onLine:false};
	    g.coords[0] = ((t+dt) * d[0]) + a[0];
	    g.coords[1] = ((t+dt) * d[1]) + a[1];
	    // check if g lies on the line
	    g.onLine = is_on(a,b,g.coords);

		return (f.onLine || g.onLine);

	} else if (parseInt(eDistCtoE) === parseInt(c[2])) {
		// console.log("Only one intersection");
		return is_on(a,b,e);
	} else {
		// console.log("No intersection");
		return false;
	}
}

function pointAlongLineInDistanceFrom(a, b, d) {
	// calculate coordinates of point distanced from a of d,
	// in direction that point b is located
	var v = [b.x - a.x, b.y - a.y];
	var v_length = Math.sqrt(v[0]*v[0] + v[1]*v[1]);
	var v_normalized = [v[0] / v_length, v[1] / v_length];
	return {
		x: a.x + d*v_normalized[0],
		y: a.y + d*v_normalized[1],
	};
}

function *getBresenhamLinePoints(a, b) {
    // Translate coordinates
    var x1 = Math.floor(a.x);
    var y1 = Math.floor(a.y);
    var x2 = Math.floor(b.x);
    var y2 = Math.floor(b.y);
    // Define differences and error check
    var dx = Math.abs(x2 - x1);
    var dy = Math.abs(y2 - y1);
    var sx = (x1 < x2) ? 1 : -1;
    var sy = (y1 < y2) ? 1 : -1;
    var err = dx - dy;
    // Set first coordinates
    yield {x: x1, y: y1};
    // Main loop
    while (!((x1 == x2) && (y1 == y2))) {
		var e2 = err << 1;
		if (e2 > -dy) {
			err -= dy;
			x1 += sx;
		}
		if (e2 < dx) {
			err += dx;
			y1 += sy;
		}
		// Set coordinates
		yield {x: x1, y: y1};
    }
}

// BASIC GEOMETRIC functions
function distance(a,b) {
	return Math.sqrt( Math.pow(a[0]-b[0], 2) + Math.pow(a[1]-b[1], 2) )
}
function is_on(a, b, c) {
	return distance(a,c) + distance(c,b) == distance(a,b);
}

function getAngles(a, b, c) {
	// calculate the angle between ab and ac
	angleAB = Math.atan2( b[1] - a[1], b[0] - a[0] );
	angleAC = Math.atan2( c[1] - a[1], c[0] - a[0] );
	angleBC = Math.atan2( b[1] - c[1], b[0] - c[0] );
	angleA = Math.abs((angleAB - angleAC) * (180/Math.PI));
	angleB = Math.abs((angleAB - angleBC) * (180/Math.PI));
	return [angleA, angleB];
}

// UTILS functions
function formatRGBA(color) {
	if (color.a == undefined) {
		color.a = 1;
	}
	return "rgba(" + color.r + ", " + color.g + ", " + color.b + ", " + color.a + ")"
}

