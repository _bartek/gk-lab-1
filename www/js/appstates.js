class StateEvent {
	constructor(x, y) {
		this.x = x;
		this.y = y;
		this.item = null;
		this.propagate = true;
	}

	stopPropagation() {
		this.propagate = false;
	}
}


class State {
	constructor(wrapper) {
		this.wrapper = wrapper;
		this.event_handlers = {
			'canvas': {
				'click': [],
				'mouseup': [],
				'mousedown': [],
				'mousemove': [],
				'rightclick': [],
			},
			'vertex': {
				'click': [],
				'mousedown': [],
				'mouseup': [],
				'mousemove': [],
				'rightclick': [],
			},
			'edge': {
				'click': [],
				'mousedown': [],
				'mouseup': [],
				'mousemove': [],
				'rightclick': [],
			},
			'polygon': {
				'click': [],
				'mousedown': [],
				'mouseup': [],
				'mousemove': [],
				'rightclick': [],
			}
		}
	}

	addEventListener(event_name, selector, callback) {
		/* called by subclasses constructors to register custom handlers */
		var handler = callback.bind(this);
		this.event_handlers[selector][event_name].push(handler);
	}

	triggerEvent(event_name, selector, event_data) {
		/* called by this.wrapper */
		var handlers = this.event_handlers[selector][event_name];
		for (var handler of handlers) {
			handler(event_data);
		}
	}

	exit() {
		this.wrapper.exitCurrentState();
	}
}


class IdleState extends State {

}


class AddPolygonState extends State {
	constructor(wrapper) {
		super(wrapper);
		
		this.polygon = new Polygon();
		this.wrapper.addElement(this.polygon);

		/* events listeners */
		this.addEventListener('click', 'canvas', this.canvasClickHandler);
		this.addEventListener('click', 'vertex', this.vertexClickHandler);
	}

	canvasClickHandler(e) {
		var vertex = new Vertex(e.x, e.y);
		this.polygon.addVertex(vertex);
	}

	vertexClickHandler(e) {
		e.stopPropagation();
		var v = e.item;
		if (this.polygon.isFirstVertex(v)) {
			this.polygon.closed = true;
			this.polygon.addLastEdge();
			this.exit();
		}
	}
}


class RemovePolygonState extends State {
	constructor(wrapper) {
		super(wrapper);
		
		/* events listeners */
		this.addEventListener('click', 'polygon', this.polygonClickHandler);
	}

	polygonClickHandler(e) {
		var polygon = e.item;
		this.wrapper.removeElement(polygon);
		this.exit();
	}
}


class RemoveVertexState extends State {
	constructor(wrapper) {
		super(wrapper);

		this.vertex = null;

		/* events listeners */
		this.addEventListener('click', 'vertex', this.vertexClickHandler);
		this.addEventListener('click', 'polygon', this.polygonClickHandler);
	}

	vertexClickHandler(e) {
		this.vertex = e.item;
	}
	polygonClickHandler(e) {
		if (this.vertex) {
			var polygon = e.item;
			polygon.removeVertex(this.vertex);
			this.exit();
		}
	}
}


class MoveVertexState extends State {
	constructor(wrapper) {
		super(wrapper);

		this.vertex = null;

		/* events listeners */
		this.addEventListener('mousedown', 'vertex', this.vertexMouseDownHandler);
		this.addEventListener('mouseup', 'canvas', this.canvasMouseUpHandler);
		this.addEventListener('mousemove', 'canvas', this.canvasMouseMoveHandler);
	}

	vertexMouseDownHandler(e) {
		this.vertex = e.item;
	}
	canvasMouseUpHandler(e) {
		if (this.vertex) {
			this.exit();
		}
	}
	canvasMouseMoveHandler(e) {
		if (this.vertex) {
			this.vertex.x = Math.floor(e.x);
			this.vertex.y = Math.floor(e.y);
		}
	}
}


class MovePolygonState extends State {
	constructor(wrapper) {
		super(wrapper);

		this.polygon = null;
		this.currentX = null;
		this.currentY = null;

		/* events listeners */
		this.addEventListener('mousedown', 'polygon', this.polygonMouseDownHandler);
		this.addEventListener('mouseup', 'canvas', this.canvasMouseUpHandler);
		this.addEventListener('mousemove', 'canvas', this.canvasMouseMoveHandler);
	}

	polygonMouseDownHandler(e) {
		this.polygon = e.item;
		this.currentX = e.x;
		this.currentY = e.y;
	}
	canvasMouseUpHandler(e) {
		if (this.polygon) {
			this.exit();
		}
	}
	canvasMouseMoveHandler(e) {
		if (this.polygon) {
			this.polygon.moveBy(
				e.x - this.currentX, e.y - this.currentY
			);
			this.currentX = e.x;
			this.currentY = e.y;
		}
	}
}


class AddVertexState extends State {
	constructor(wrapper) {
		super(wrapper);

		this.edge = null;

		/* events listeners */
		this.addEventListener('click', 'edge', this.edgeClickHandler);
		this.addEventListener('click', 'polygon', this.polygonClickHandler);
	}

	edgeClickHandler(e) {
		this.edge = e.item;
	}

	polygonClickHandler(e) {
		if (this.edge) {
			var polygon = e.item;
			var position = polygon.vertices.indexOf(this.edge.v1);
			polygon.addVertex(this.edge.center, position);
			this.exit();
		}
	}
}

class AddVerticalLineRestrictionState extends State {
	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'edge', this.edgeClickHandler);
	}

	edgeClickHandler(e) {
		var edge = e.item;
		edge.restriction = new VerticalLineRestriction(edge);
		this.exit();
	}
}

class AddHorizontalLineRestrictionState extends State {
	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'edge', this.edgeClickHandler);
	}

	edgeClickHandler(e) {
		var edge = e.item;
		edge.restriction = new HorizontalLineRestriction(edge);
		this.exit();
	}
}

class AddFixedLengthLineRestrictionState extends State {
	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'edge', this.edgeClickHandler);
	}

	edgeClickHandler(e) {
		var edge = e.item;
		var length = parseInt(prompt("Wprowadź długość"));
		if (length) {
			edge.restriction = new FixedLengthLineRestriction(edge, length);
			this.exit();
		} else {
			alert("Niepoprawna wartość!");
		}
	}
}

class RemoveRestrictionState extends State {
	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'edge', this.edgeClickHandler);
	}

	edgeClickHandler(e) {
		var edge = e.item;
		edge.restriction = null;
		this.exit();
	}
}

class SetBresenhamAlgorithmState extends State {
	get algorithm() {
		return BresenhamAlgorithm;
	}

	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'polygon', this.polygonClickHandler);
	}

	polygonClickHandler(e) {
		var polygon = e.item;
		polygon.lineAlgorithm = this.algorithm;
		this.exit();
	}
}

class SetXiaolinWuAlgorithmState extends SetBresenhamAlgorithmState {
	get algorithm() {
		return XiaolinWuAlgorithm;
	}
}

class SetEmbeddedAlgorithmState extends SetBresenhamAlgorithmState {
	get algorithm() {
		return EmbeddedAlgorithm;
	}
}

class ClipPolygonState extends State {
	constructor(wrapper) {
		super(wrapper);

		this.subjectPolygon = null;

		/* events listeners */
		this.addEventListener('click', 'polygon', this.polygonClickHandler);
	}

	polygonClickHandler(e) {
		var polygon = e.item;
		if (this.subjectPolygon) {
			var clippedPolygon = this.subjectPolygon.clip(polygon);
			this.wrapper.removeElement(this.subjectPolygon);
			this.wrapper.removeElement(polygon);
			this.wrapper.addElement(clippedPolygon);
			this.exit();
		} else {
			this.subjectPolygon = polygon;
		}
	}
}

class FillPolygonState extends State {
	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'polygon', this.polygonClickHandler);
	}

	polygonClickHandler(e) {
		var polygon = e.item;
		polygon.filled = !polygon.filled;
		this.exit();
	}
}

class AddLightState extends State {
	constructor(wrapper) {
		super(wrapper);

		this.height = prompt("Podaj wysokość światła (np. 75):");

		/* events listeners */
		this.addEventListener('click', 'canvas', this.canvasClickHandler);
		this.addEventListener('rightclick', 'canvas', this.canvasRightClickHandler);
	}

	canvasClickHandler(e) {
		var light = new Light({r: 255, g: 255, b: 255}, [e.x, e.y, this.height]);
		this.wrapper.texture.light = light;
		this.wrapper.texture.update();
	}

	canvasRightClickHandler(e) {
		this.exit();
	}
}

class RemoveLightState extends State {
	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'canvas', this.canvasClickHandler);
	}

	canvasClickHandler(e) {
		this.wrapper.texture.light = null;
		this.wrapper.texture.update();
		this.exit();
	}
}

class AddBubbleState extends State {

	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'canvas', this.canvasClickHandler);
		this.addEventListener('rightclick', 'canvas', this.canvasRightClickHandler);
	}

	canvasClickHandler(e) {
		var surface = new Surface(e.x, e.y, 50);
		this.wrapper.texture.surface = surface;
		this.wrapper.texture.update();
	}

	canvasRightClickHandler(e) {
		this.exit();
	}
}

class RemoveBubbleState extends State {

	constructor(wrapper) {
		super(wrapper);

		/* events listeners */
		this.addEventListener('click', 'canvas', this.canvasClickHandler);
	}

	canvasClickHandler(e) {
		this.wrapper.texture.surface = null;
		this.wrapper.texture.update();
		this.exit();
	}
}