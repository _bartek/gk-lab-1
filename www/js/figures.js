class Vertex {
    /* class representing single vertex */
    
    static get WIDTH() {
        return 10;
    }
    static get HEIGHT() {
        return 10;
    }

    constructor(x,y) {
        this.x = Math.floor(x);
        this.y = Math.floor(y);
    }
    
    render(ctx) {
        /* draws itself on ctx */
        ctx.fillRect(
            this.hitBoxBoundary.left, this.hitBoxBoundary.top,
            Vertex.WIDTH, Vertex.HEIGHT
        );
    }

    get hitBoxBoundary() {
        return {
            left: this.x - Vertex.WIDTH/2,
            right: this.x + Vertex.WIDTH/2,
            top: this.y - Vertex.HEIGHT/2,
            bottom: this.y + Vertex.HEIGHT/2,
        }
    }

    hitBoxContains(x, y) {
        var boundary = this.hitBoxBoundary;
        if (
            (x >= boundary.left && x <= boundary.right) &&
            (y >= boundary.top && y <= boundary.bottom)
        ) {
            return true;
        } else {
            return false;
        }
    }
}

class Edge {
    /* class representing edge between two points */

    constructor(v1, v2) {
        this.v1 = v1;
        this.v2 = v2;
        this.restriction = undefined;
    }

    get center() {
        var dx = Math.abs(this.v1.x - this.v2.x);
        var dy = Math.abs(this.v1.y - this.v2.y);

        return new Vertex(
            Math.max(this.v1.x, this.v2.x) - dx/2,
            Math.max(this.v1.y, this.v2.y) - dy/2
        );
    }

    get length() {
        var dx = Math.abs(this.v1.x - this.v2.x);
        var dy = Math.abs(this.v1.y - this.v2.y);
        return Math.sqrt(dx*dx + dy*dy);
    }

    render(ctx, algorithm) {
        var color = {r: 0, g: 0, b: 0};

        if (this.restriction) {
            color = {r: 0, g: 0, b: 255};
            ctx.fillText(this.restriction.label, this.center.x, this.center.y);
        }

        var alg = new algorithm(ctx);
        alg.drawEdge(this, color);
    }

    hitBoxContains(x, y) {
        var hitboxRadius = 5;
        return segmentIntersectsCircle(
            [this.v1.x, this.v1.y], [this.v2.x, this.v2.y], [x, y, hitboxRadius]
        );
    }

    isValid() {
        if (this.restriction) {
            return this.restriction.isValid();
        }
        return true;
    }    
}


class Polygon {
    /* class representing polygon composed of vertices */

    constructor(vertices) {
        this.vertices = [];
        this.edges = [];
        this.closed = false;
        this.filled = false;
        this.lineAlgorithm = EmbeddedAlgorithm;

        if (vertices) {
            for (var v of vertices) {
                this.addVertex(v);
            }
            this.addLastEdge();
            this.closed = true;
        }
    }

    addVertex(v, position = null) {
        // position - idx of vertex that should precede added vertex
        // defaults to last vertex
        if (position == null) {
            position = this.vertices.length - 1;
        }

        var predecessor = this.vertices[position];
        if (predecessor) {
            var newEdge;
            var edgeToDivide = this.getEdgesForVertex(predecessor).right;
            if (edgeToDivide) {
                newEdge = new Edge(v, edgeToDivide.v2);
                edgeToDivide.v2 = v;
                edgeToDivide.restriction = null;
            }
            else {
                newEdge = new Edge(predecessor, v);
            }
            this.edges.splice(position + 1, 0, newEdge);
        }

        this.vertices.splice(position + 1, 0, v);
    }

    getEdgesForVertex(v) {
        var idx = this.vertices.indexOf(v);
        var n = this.vertices.length;
        var edgeRightIdx = idx % n;
        var edgeLeftIdx = (((idx-1)%n)+n)%n; // idx-1 might be -

        return {
            left: this.edges[edgeLeftIdx],
            right: this.edges[edgeRightIdx]
        }
    }

    removeVertex(v) {
        var idx = this.vertices.indexOf(v);
        if (idx > -1) {
            // removing one redundant edge
            var edges = this.getEdgesForVertex(v);
            edges.left.v2 = edges.right.v2;
            edges.left.restriction = undefined;
            this.edges.splice(this.edges.indexOf(edges.right), 1);

            // removing vertex itself
            this.vertices.splice(idx, 1);
        }
    }

    isFirstVertex(v) {
        /* checks if given vertex was first vertex added to that polygon */
        var idx = this.vertices.indexOf(v);
        return idx == 0 ? true : false;
    }

    isLastVertex(v) {
        var idx = this.vertices.indexOf(v);
        return idx == this.vertices.length - 1 ? true : false;
    }

    moveBy(dx, dy) {
        /* moves entire polygon (every vertex) by x and y */
        for (var vertex of this.vertices) {
            vertex.x += dx;
            vertex.y += dy;
        }
    }

    addLastEdge() {
        var firstVertex = _.first(this.vertices);
        var lastVertex = _.last(this.vertices);
        this.edges.push(new Edge(lastVertex, firstVertex));
    }

    render(ctx, texture) {
        /* draws whole polygon on ctx */
        this.validate();

        if (this.vertices.length) {
            ctx.fillText(
                this.lineAlgorithm.label, this.vertices[0].x + 10,
                this.vertices[0].y + 10
            );
        }

        if (this.filled) {
            this.fill(ctx, texture);
        }

        for (var vertex of this.vertices) {
            vertex.render(ctx);
        }
        for (var edge of this.edges) {
            edge.render(ctx, this.lineAlgorithm);
        }
    }

    clip(clipPolygon) {
        var cp1, cp2, s, e;
        var inside = function (p) {
            return (cp2.x-cp1.x)*(p.y-cp1.y) > (cp2.y-cp1.y)*(p.x-cp1.x);
        };
        var intersection = function () {
            var dc = {x: cp1.x - cp2.x, y: cp1.y - cp2.y},
                dp = {x: s.x - e.x, y: s.y - e.y },
                n1 = cp1.x * cp2.y - cp1.y * cp2.x,
                n2 = s.x * e.y - s.y * e.x, 
                n3 = 1.0 / (dc.x * dp.y - dc.y * dp.x);
            return new Vertex((n1*dp.x - n2*dc.x) * n3, (n1*dp.y - n2*dc.y) * n3);
        };
        var outputList = this.vertices;
        cp1 = _.last(clipPolygon.vertices);
        for (var cp2 of clipPolygon.vertices) {
            var inputList = outputList;
            outputList = [];
            s = _.last(inputList); //last on the input list
            for (var e of inputList) {
                if (inside(e)) {
                    if (!inside(s)) {
                        outputList.push(intersection());
                    }
                    outputList.push(e);
                }
                else if (inside(s)) {
                    outputList.push(intersection());
                }
                s = e;
            }
            cp1 = cp2;
        }
        return new Polygon(outputList);
    }

    fill(ctx, texture) {
        /* fills polygon with given color */
        var prevStyle = ctx.fillStyle;

        var indexes = _.range(this.vertices.length);
        var vertices = this.vertices;
        indexes = _.sortBy(indexes, function (idx) {
            return vertices[idx].y;
        });
        var yMin = vertices[_.first(indexes)].y;
        var yMax = vertices[_.last(indexes)].y;

        var aet = [];

        // for each scanline
        for (var scanLine = yMin; scanLine <= yMax; scanLine++) {
            // for each vertex that is just below the scanLine
            for (var idx of indexes) {
                var vertex = this.vertices[idx];
                if (vertex.y == scanLine - 1) {
                    var edges = this.getEdgesForVertex(vertex);
                    var previousVertex = edges.left.v1;
                    var nextVertex = edges.right.v2;
                    if (previousVertex.y > vertex.y) {
                        aet.push({
                            x: vertex.x,
                            yMax: previousVertex.y,
                            d: (previousVertex.x - vertex.x) / (previousVertex.y - vertex.y)
                        });
                    } else if (previousVertex.y < vertex.y) {
                        aet = _.filter(aet, function (edge) {
                            return edge.yMax != scanLine - 1;
                        });
                    }
                    if (nextVertex.y > vertex.y) {
                        aet.push({
                            x: vertex.x,
                            yMax: nextVertex.y,
                            d: (nextVertex.x - vertex.x) / (nextVertex.y - vertex.y)
                        });
                    } else if (nextVertex.y < vertex.y) {
                        aet = _.filter(aet, function (edge) {
                            return edge.yMax != scanLine - 1;
                        });
                    }
                }
            }
            //
            aet = _.sortBy(aet, function (edge) { return edge.x; });
            for (var i = 0; i < aet.length; i += 2) {
                if (i + 1 < aet.length) {
                    var x1 = Math.max(Math.floor(aet[i].x), 0);
                    var x2 = Math.min(Math.ceil(aet[i+1].x), 500);
                    
                    if (x1 != x2) {
                        ctx.putImageData(texture.getImageData(x1, scanLine, x2-x1, 1), x1, scanLine);
                    }
                    //lineDrawer.drawEdge(new Edge(p1, p2), color);
                }
            }
            for (var edge of aet) {
                edge.x += edge.d;
            }
        }
        ctx.fillStyle = prevStyle;
    }

    hitBoxContains(x, y) {
        if (!this.closed) {
            return false;
        }

        var n = this.vertices.length;
        var contains = false;

        for (var i=0, j=n-1; i<n; j=i++) {
            if (
                ((this.vertices[i].y>y) != (this.vertices[j].y>y)) &&
                (x < (this.vertices[j].x-this.vertices[i].x) * (y-this.vertices[i].y) / (this.vertices[j].y-this.vertices[i].y) + this.vertices[i].x) 
            ) {
                contains = !contains;
            }
        }
        return contains;
    }

    getElementsForPosition(x, y) {
        var elements = [{
            item: this,
            selector: 'polygon',
        }];

        // check all vertices
        for (var vertex of this.vertices) {
            if (vertex.hitBoxContains(x, y)) {
                elements.push({
                    item: vertex,
                    selector: 'vertex',
                });
                return elements;
            }
        }
        // check all edges
        for (var edge of this.edges) {
            if (edge.hitBoxContains(x, y)) {
                elements.push({
                    item: edge,
                    selector: 'edge',
                });
                return elements;
            }
        }

        // check itself
        if (this.hitBoxContains(x, y)) {
            return elements;
        }

        // return empty list, since given point is outside of the polygon
        return [];
    }

    validate() {
        for (var edge of this.edges) {
            if (!edge.isValid()) {
                edge.restriction.makeValid();
            }
        }
    }
}
