class Surface {
	constructor(bubbleX, bubbleY, bubbleR) {
		this.x0 = bubbleX;
		this.y0 = bubbleY;
		this.r = bubbleR;
	}

	normalizeVector(vector) {
		var norm = numeric.norm2(vector);
		return numeric.div(vector, norm);
	}

	calcNormalVector(x, y) {
		var z2 = (this.r*this.r - (x - this.x0)*(x - this.x0) - (y - this.y0)*(y - this.y0));
		if (z2 < 0) {
			return [0, 0, 1];
		} else {
			var z = Math.sqrt(z2);
			return this.normalizeVector(numeric.sub([x, y, z], [this.x0, this.y0, 0]));
		}
	}
}

class Light {
	constructor(color, position, surface) {
		this.color = color;
		this.position = position;
		this.surface = surface;
	}

	sanitize(val, min, max) {
		return Math.floor(Math.min(Math.max(val, min), max));
	}

	illuminatePixel(x, y, pixelColor) {
		var N = this.getNormalVector(x, y);
		var L = numeric.sub(this.position, [x, y, 0]);
		var cos = this.calcCosinusBetween(N, L) / 255;
		return {
			r: this.sanitize(this.color.r * pixelColor.r * cos * 2, 0, 255),
			g: this.sanitize(this.color.g * pixelColor.g * cos * 2, 0, 255),
			b: this.sanitize(this.color.b * pixelColor.b * cos * 2, 0, 255)
		}
	}

	normalizeVector(vector) {
		var norm = numeric.norm2(vector);
		return numeric.div(vector, norm);
	}

	calcCosinusBetween(vector1, vector2) {
		return numeric.sum(numeric.mul(
			this.normalizeVector(vector1),
			this.normalizeVector(vector2)
		));
	}

	getNormalVector(x, y) {
		if (!this.surface) {
			return [0, 0, 1];
		} else {
			return this.surface.calcNormalVector(x, y);
		}
	}
}


class Texture {
	constructor(filename, width, height) {
		this.width = width;
		this.height = height;

		this.loadTexture(filename);	
	}

    loadTexture(filename) {
        var canvas = $('<canvas width="' + this.width + '" height="' + this.height + '">')[0];
        var img = $('<img src="' + filename + '" width="64" height="64">')[0];

        var self = this;
        img.onload = function() {
        	var textureContext = canvas.getContext("2d");
        	var pattern = textureContext.createPattern(img, "repeat");
        	textureContext.clearRect(0, 0, self.width, self.height);
        	textureContext.rect(0, 0, self.width, self.height);
        	textureContext.fillStyle = pattern;
        	textureContext.fill();

        	self.data = textureContext.getImageData(0, 0, self.width, self.height).data;
        	self.ctx = textureContext;
        	console.log("Texture " + filename + " loaded.");
        }
    }

	getImageData(x, y, width, height) {
		var start = y*this.height*4 + x*4;
		var end = start + width*height*4;
		var dataSource;

		if (!this.light) {
			dataSource = this.data;
		} else {
			dataSource = this.updatedData;
		}
		var data = dataSource.slice(start, end);
		var imageData = this.ctx.createImageData(width, height);
		imageData.data.set(data);
		return imageData;
	}

	update() {
		if (!this.light) {
			return;
		}
		this.light.surface = this.surface;

		this.updatedData = this.ctx.getImageData(0, 0, this.width, this.height).data;

		for (var x = 0; x < this.width; x++) {
			for (var y = 0; y < this.height; y++) {
				var offset = y*this.height*4 + x*4;
				var currentColor = {
					r: this.data[offset],
					g: this.data[offset+1],
					b: this.data[offset+2]
				}
				var newColor = this.light.illuminatePixel(x, y, currentColor);
				this.updatedData[offset] = newColor.r;
				this.updatedData[offset+1] = newColor.g;
				this.updatedData[offset+2] = newColor.b;
			}
		}
		console.log("Texture updated with lightning");
	}
}