class LineAlgorithm {
	constructor(ctx) {
		this.ctx = ctx;
	}


	drawEdge(edge, color) {
		var prevStyle = this.ctx.fillStyle;
		this.ctx.fillStyle = formatRGBA(color);

		var pixels = this.getPoints(edge.v1, edge.v2);
		for (var pixel of pixels) {
			this.setPixel(pixel, color);
		}

		this.ctx.fillStyle = prevStyle;
	}

	setPixel(pixel) {
		this.ctx.fillRect(pixel.x, pixel.y, 1, 1);
	}

	getPoints(a, b) {
		return this._getPoints(a,b);
	}
}


class EmbeddedAlgorithm extends LineAlgorithm {
	static get label() {
		return 'E';
	}

	drawEdge(edge, color) {
		var prevStyle = this.ctx.strokeStyle;
		this.ctx.beginPath();
		this.ctx.moveTo(edge.v1.x, edge.v1.y);
		this.ctx.lineTo(edge.v2.x, edge.v2.y);
		this.ctx.strokeStyle = formatRGBA(color);
		this.ctx.stroke();
		this.ctx.strokeStyle = prevStyle;
	}
}

class BresenhamAlgorithm extends LineAlgorithm {

	static get label() {
		return 'B';
	}

	constructor(ctx) {
		super(ctx);
		// it's super ugly, but ES6 syntax probably doesn't support
		// embeding generators as class methods.
		this._getPoints = function*(a,b) {
			// Translate coordinates
		    var x1 = Math.floor(a.x);
		    var y1 = Math.floor(a.y);
		    var x2 = Math.floor(b.x);
		    var y2 = Math.floor(b.y);
		    // Define differences and error check
		    var dx = Math.abs(x2 - x1);
		    var dy = Math.abs(y2 - y1);
		    var sx = (x1 < x2) ? 1 : -1;
		    var sy = (y1 < y2) ? 1 : -1;
		    var err = dx - dy;
		    // Set first coordinates
		    yield {x: x1, y: y1};
		    // Main loop
		    while (!((x1 == x2) && (y1 == y2))) {
				var e2 = err << 1;
				if (e2 > -dy) {
					err -= dy;
					x1 += sx;
				}
				if (e2 < dx) {
					err += dx;
					y1 += sy;
				}
				// Set coordinates
				yield {x: x1, y: y1};
		    }
		}
	}
}


class XiaolinWuAlgorithm extends LineAlgorithm {

	static get label() {
		return 'XW';
	}

	constructor(ctx) {
		super(ctx);
		// it's super ugly, but ES6 syntax probably doesn't support
		// embeding generators as class methods.
		this._getPoints = function*(a, b) {
			var getPixel = function(swapAxes, x, y, c) { 
		    	if (swapAxes)
		    		return {x: y, y: x, c: c}; 
		    	else 
		    		return {x: x, y: y, c: c};
		    }

		    var ipart = function(x) {
		    	return Math.floor(x);
		    }

		    var fpart = function(x) {
		    	return x - Math.floor(x);
		    }

		    var rfpart = function(x) {
		    	return 1 - fpart(x);
		    }

		    // Translate coordinates
		    var x1 = Math.floor(a.x);
		    var y1 = Math.floor(a.y);
		    var x2 = Math.floor(b.x);
		    var y2 = Math.floor(b.y);

		    // actual algorithm
		    var dx = x2 - x1;
		    var dy = y2 - y1;
		    var swapAxes = false;

		    if (Math.abs(dx) < Math.abs(dy)) {    
		        swapAxes=true;             
		        var t;
		        t = x1; x1 = y1; y1 = t;
		        t = x2; x2 = y2; y2 = t;
		        t = dx; dx = dy; dy = t;
		        //swap x1, y1
		        //swap x2, y2
		        //swap dx, dy
		    }
		    if (x2 < x1) {
		        //swap x1, x2
		        //swap y1, y2
		        t = x1; x1 = x2; x2 = t;
		        t = y1; y1 = y2; y2 = t;
		    }
		    var gradient = dy / dx;

		    // handle first endpoint
		    var xend = Math.round(x1);
		    var yend = y1 + gradient * (xend - x1);
		    var xgap = rfpart(x1 + 0.5);
		    var xpxl1 = xend;  // this will be used in the main loop
		    var ypxl1 = ipart(yend);

		    yield getPixel(swapAxes, xpxl1, ypxl1, rfpart(yend) * xgap);
		    yield getPixel(swapAxes, xpxl1, ypxl1 + 1, fpart(yend) * xgap);
		    var intery = yend + gradient; // first y-intersection for the main loop

		    // handle second endpoint
		    xend = Math.round(x2);
		    yend = y2 + gradient * (xend - x2);
		    xgap = fpart(x2 + 0.5);
		    var xpxl2 = xend;  // this will be used in the main loop
		    var ypxl2 = ipart(yend);

		    yield getPixel(swapAxes, xpxl2, ypxl2, rfpart(yend) * xgap);
		    yield getPixel(swapAxes, xpxl2, ypxl2 + 1, fpart(yend) * xgap);

		    // main loop
		    for (var x = xpxl1 + 1; x <= xpxl2 - 1; x++) {
		        yield getPixel(swapAxes, x, ipart(intery), rfpart(intery));
		        yield getPixel(swapAxes, x, ipart(intery) + 1, fpart(intery));
		        intery = intery + gradient;
		    }
		}
	}

	setPixel(pixel, color) {
		color.a = pixel.c;
		this.ctx.fillStyle = formatRGBA(color);
		super.setPixel(pixel);
	}
}

