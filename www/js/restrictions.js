class VerticalLineRestriction {

	constructor(edge) {
		this.edge = edge;
		this.label = 'V';
	}

	isValid() {
		return (this.edge.v1.x == this.edge.v2.x);
	}

	makeValid() {
		this.edge.v2.x = this.edge.v1.x;
	}
}

class HorizontalLineRestriction {
	constructor(edge) {
		this.edge = edge;
		this.label = 'H';
	}

	isValid() {
		return (this.edge.v1.y == this.edge.v2.y);
	}

	makeValid() {
		this.edge.v2.y = this.edge.v1.y;
	}
}

class FixedLengthLineRestriction {
	constructor(edge, length) {
		this.edge = edge;
		this.length = length;
		this.label = length.toString();
	}

	isValid() {
		var v1 = this.edge.v1;
		var v2 = this.edge.v2;
		return (distance([v1.x, v1.y], [v2.x, v2.y]) == this.length);
	}

	makeValid() {
		var v1 = this.edge.v1;
		var v2 = this.edge.v2;
		var d = pointAlongLineInDistanceFrom(
			this.edge.v1, this.edge.v2, this.length
		);
		v2.x = d.x;
		v2.y = d.y;
	}
}