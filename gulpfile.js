var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

var paths = {
	sass: './www/styles/sass/**/*.scss',
	css: './www/styles/css/',
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
	gulp.src(paths.sass)
	    .pipe(sass())
	    .pipe(concat('styles.css'))
	    .on('error', sass.logError)
	    .pipe(gulp.dest(paths.css))
	    .on('end', done);
});

gulp.task('watch', function() {
	gulp.watch([paths.sass], ['sass']);
});